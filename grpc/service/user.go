package service

import (
	"context"
	"delivery/user_service/config"
	"delivery/user_service/genproto/user_service"
	"delivery/user_service/grpc/client"
	"delivery/user_service/pkg/logger"
	"delivery/user_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedUserServiceServer
}

func NewUserService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *UserService {
	return &UserService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *UserService) Create(ctx context.Context, req *user_service.UserCreateReq) (*user_service.UserCreateResp, error) {
	u.log.Info("======  User Create ======", logger.Any("req", req))

	resp, err := u.strg.User().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating  User", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *UserService) GetList(ctx context.Context, req *user_service.UserGetListReq) (*user_service.UserGetListResp, error) {
	u.log.Info("======  User GetList ======", logger.Any("req", req))

	resp, err := u.strg.User().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getting  User", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *UserService) GetById(ctx context.Context, req *user_service.UserIdReq) (*user_service.User, error) {
	u.log.Info("======  User GetById ======", logger.Any("req", req))

	resp, err := u.strg.User().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while getting User", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *UserService) Update(ctx context.Context, req *user_service.UserUpdateReq) (*user_service.UserUpdateResp, error) {
	u.log.Info("======  User Update ======", logger.Any("req", req))

	resp, err := u.strg.User().Update(ctx, req)
	if err != nil {
		u.log.Error("error while updating  User", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *UserService) Delete(ctx context.Context, req *user_service.UserIdReq) (*user_service.UserDeleteResp, error) {
	u.log.Info("======  User Delete ======", logger.Any("req", req))

	resp, err := u.strg.User().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while deleting User", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
