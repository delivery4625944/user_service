package service

import (
	"context"
	"delivery/user_service/config"
	"delivery/user_service/genproto/user_service"
	"delivery/user_service/grpc/client"
	"delivery/user_service/pkg/logger"
	"delivery/user_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CourierService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedCourierServiceServer
}

func NewCourierService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *CourierService {
	return &CourierService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *CourierService) Create(ctx context.Context, req *user_service.CourierCreateReq) (*user_service.CourierCreateResp, error) {
	u.log.Info("====== Courier Create ======", logger.Any("req", req))

	resp, err := u.strg.Courier().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating Courier", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CourierService) GetList(ctx context.Context, req *user_service.CourierGetListReq) (*user_service.CourierGetListResp, error) {
	u.log.Info("====== Courier GetList ======", logger.Any("req", req))

	resp, err := u.strg.Courier().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getting Courier", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CourierService) GetById(ctx context.Context, req *user_service.CourierIdReq) (*user_service.Courier, error) {
	u.log.Info("====== Courier GetById ======", logger.Any("req", req))

	resp, err := u.strg.Courier().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while getting Courier", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CourierService) Update(ctx context.Context, req *user_service.CourierUpdateReq) (*user_service.CourierUpdateResp, error) {
	u.log.Info("====== Courier Update ======", logger.Any("req", req))

	resp, err := u.strg.Courier().Update(ctx, req)
	if err != nil {
		u.log.Error("error while updating Courier", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CourierService) Delete(ctx context.Context, req *user_service.CourierIdReq) (*user_service.CourierDeleteResp, error) {
	u.log.Info("====== Courier Delete ======", logger.Any("req", req))

	resp, err := u.strg.Courier().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while deleting Courier", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
