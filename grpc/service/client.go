package service

import (
	"context"
	"delivery/user_service/config"
	"delivery/user_service/genproto/user_service"
	"delivery/user_service/grpc/client"
	"delivery/user_service/pkg/logger"
	"delivery/user_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ClientService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedClientServiceServer
}

func NewClientService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ClientService {
	return &ClientService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (s *ClientService) Create(ctx context.Context, req *user_service.ClientCreateReq) (*user_service.ClientCreateResp, error) {
	s.log.Info("====== Client Create ======", logger.Any("req", req))
	resp, err := s.strg.Client().Create(ctx, req)
	if err != nil {
		s.log.Error("error while creating Client", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (s *ClientService) GetList(ctx context.Context, req *user_service.ClientGetListReq) (*user_service.ClientGetListResp, error) {
	s.log.Info("====== client GetList ======", logger.Any("req", req))
	resp, err := s.strg.Client().GetList(ctx, req)
	if err != nil {
		s.log.Error("error while getting client", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (s *ClientService) GetById(ctx context.Context, req *user_service.ClientIdReq) (*user_service.Client, error) {
	s.log.Info("====== client GetById ======", logger.Any("req", req))
	resp, err := s.strg.Client().GetById(ctx, req)
	if err != nil {
		s.log.Error("error while getting client", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (s *ClientService) Update(ctx context.Context, req *user_service.ClientUpdateReq) (*user_service.ClientUpdateResp, error) {
	s.log.Info("====== client Update ======", logger.Any("req", req))
	resp, err := s.strg.Client().Update(ctx, req)
	if err != nil {
		s.log.Error("error while updating client", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (s *ClientService) Delete(ctx context.Context, req *user_service.ClientIdReq) (*user_service.ClientDeleteResp, error) {
	s.log.Info("====== client Delete ======", logger.Any("req", req))
	resp, err := s.strg.Client().Delete(ctx, req)
	if err != nil {
		s.log.Error("error while deleting client", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
