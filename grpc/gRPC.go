package grpc

import (
	"delivery/user_service/config"
	"delivery/user_service/genproto/user_service"
	"delivery/user_service/grpc/client"
	"delivery/user_service/grpc/service"
	"delivery/user_service/pkg/logger"
	"delivery/user_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	user_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	user_service.RegisterClientServiceServer(grpcServer, service.NewClientService(cfg, log, strg, srvc))
	user_service.RegisterCourierServiceServer(grpcServer, service.NewCourierService(cfg, log, strg, srvc))
	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
