package storage

import (
	"context"
	"delivery/user_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	Client() ClientRepoI
	Courier() CourierRepoI
	User() UserRepoI
	Branch() BranchRepoI
}

type ClientRepoI interface {
	Create(ctx context.Context, req *user_service.ClientCreateReq) (*user_service.ClientCreateResp, error)
	GetList(ctx context.Context, req *user_service.ClientGetListReq) (*user_service.ClientGetListResp, error)
	GetById(ctx context.Context, req *user_service.ClientIdReq) (*user_service.Client, error)
	Update(ctx context.Context, req *user_service.ClientUpdateReq) (*user_service.ClientUpdateResp, error)
	Delete(ctx context.Context, req *user_service.ClientIdReq) (*user_service.ClientDeleteResp, error)
}

type CourierRepoI interface {
	Create(ctx context.Context, req *user_service.CourierCreateReq) (*user_service.CourierCreateResp, error)
	GetList(ctx context.Context, req *user_service.CourierGetListReq) (*user_service.CourierGetListResp, error)
	GetById(ctx context.Context, req *user_service.CourierIdReq) (*user_service.Courier, error)
	Update(ctx context.Context, req *user_service.CourierUpdateReq) (*user_service.CourierUpdateResp, error)
	Delete(ctx context.Context, req *user_service.CourierIdReq) (*user_service.CourierDeleteResp, error)
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.UserCreateReq) (*user_service.UserCreateResp, error)
	GetList(ctx context.Context, req *user_service.UserGetListReq) (*user_service.UserGetListResp, error)
	GetById(ctx context.Context, req *user_service.UserIdReq) (*user_service.User, error)
	Update(ctx context.Context, req *user_service.UserUpdateReq) (*user_service.UserUpdateResp, error)
	Delete(ctx context.Context, req *user_service.UserIdReq) (*user_service.UserDeleteResp, error)
}

type BranchRepoI interface {
	Create(ctx context.Context, req *user_service.BranchCreateReq) (*user_service.BranchCreateResp, error)
	GetList(ctx context.Context, req *user_service.BranchGetListReq) (*user_service.BranchGetListResp, error)
	GetById(ctx context.Context, req *user_service.BranchIdReq) (*user_service.Branch, error)
	Update(ctx context.Context, req *user_service.BranchUpdateReq) (*user_service.BranchUpdateResp, error)
	Delete(ctx context.Context, req *user_service.BranchIdReq) (*user_service.BranchDeleteResp, error)
}
