package postgres

import (
	"context"
	"delivery/user_service/genproto/user_service"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
)

type CourierRepo struct {
	db *pgxpool.Pool
}

func NewCourierRepo(db *pgxpool.Pool) *CourierRepo {
	return &CourierRepo{
		db: db,
	}
}

func (r *CourierRepo) Create(ctx context.Context, req *user_service.CourierCreateReq) (*user_service.CourierCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO couriers(
		id,
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password,
		max_order_count,
		branch_id
	)
	VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Active,
		req.Login,
		req.Password,
		req.MaxOrderCount,
		req.BranchId,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &user_service.CourierCreateResp{
		Msg: "sale created with id: " + id,
	}, nil
}

func (r *CourierRepo) GetList(ctx context.Context, req *user_service.CourierGetListReq) (*user_service.CourierGetListResp, error) {
	var (
		filter  = " WHERE TRUE "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password,
		max_order_count,
		branch_id
	FROM couriers `

	if req.FirstName != "" {
		filter += ` AND name ILIKE ` + "'%" + req.FirstName + "%' OR last_name ILIKE '%" + req.LastName + "%' "
	}
	if req.PhoneNumber != "" {
		filter += ` AND phone_number='` + req.PhoneNumber + `' `
	}
	

	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ
	// fmt.Println(query)



	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	resp := &user_service.CourierGetListResp{}
	for rows.Next() {
		var courier = user_service.Courier{}
		err := rows.Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.PhoneNumber,
			&courier.Active,
			&courier.Login,
			&courier.Password,
			&courier.MaxOrderCount,
			&courier.BranchId,
		)

		if err != nil {
			return nil, err
		}
		resp.Couriers = append(resp.Couriers, &courier)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *CourierRepo) GetById(ctx context.Context, req *user_service.CourierIdReq) (*user_service.Courier, error) {
	query := `
	SELECT 
	    id,
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password,
		max_order_count,
		branch_id
	FROM sales
	WHERE id=$1 
	`

	var courier = user_service.Courier{}
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.PhoneNumber,
			&courier.Active,
			&courier.Login,
			&courier.Password,
			&courier.MaxOrderCount,
			&courier.BranchId,
	); err != nil {
		return nil, err
	}

	return &courier, nil
}

func (r *CourierRepo) Update(ctx context.Context, req *user_service.CourierUpdateReq) (*user_service.CourierUpdateResp, error) {
	query := `
	UPDATE couriers 
	SET 
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password,
		max_order_count,
		branch_id
	WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Active,
		req.Login,
		req.Password,
		req.MaxOrderCount,
		req.BranchId,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &user_service.CourierUpdateResp{Msg: "OK"}, nil
}

func (r *CourierRepo) Delete(ctx context.Context, req *user_service.CourierIdReq) (*user_service.CourierDeleteResp, error) {
	query := `
    DELETE FROM couriers 
    WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &user_service.CourierDeleteResp{Msg: "OK"}, nil
}
