package postgres

import (
	"context"
	"delivery/user_service/genproto/user_service"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *BranchRepo {
	return &BranchRepo{
		db: db,
	}
}

func (r *BranchRepo) Create(ctx context.Context, req *user_service.BranchCreateReq) (*user_service.BranchCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO branches (
		id,
		name,
		phone_number,
		photos,
		delivery_tarif_id,
		start_time,
		end_time,
		address,
		destination,
		active
	)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.PhoneNumber,
		req.Photos,
		req.DeliveryTarifId,
		req.StartTime,
		req.EndTime,
		req.Address,
		req.Destination,
		req.Active,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &user_service.BranchCreateResp{
		Msg: "branch created with id: " + id,
	}, nil
}

func (r *BranchRepo) GetList(ctx context.Context, req *user_service.BranchGetListReq) (*user_service.BranchGetListResp, error) {
	var (
		filter  = " WHERE TRUE "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		name,
		phone_number,
		photos,
		delivery_tarif_id,
		start_time,
		end_time,
		address,
		destination,
		active,
		created_at::TEXT 
	FROM branches `

	if req.Name != "" {
		filter += ` AND name ILIKE ` + "'%" + req.Name + "%' OR address ILIKE '%" + req.Name + "%' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM branches` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &user_service.BranchGetListResp{}
	for rows.Next() {
		var branch = user_service.Branch{}
		err := rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.PhoneNumber,
			&branch.Photos,
			&branch.DeliveryTarifId,
			&branch.StartTime,
			&branch.EndTime,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
		)

		if err != nil {
			return nil, err
		}
		resp.Branches = append(resp.Branches, &branch)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *BranchRepo) GetById(ctx context.Context, req *user_service.BranchIdReq) (*user_service.Branch, error) {
	query := `
    SELECT 
		id,
		name,
		phone_number,
		photos,
		delivery_tarif_id,
		start_time,
		end_time,
		address,
		destination,
		active,
		created_at::TEXT 
    FROM branches 
    WHERE id = $1
    `
	var branch = user_service.Branch{}

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		    &branch.Id,
			&branch.Name,
			&branch.PhoneNumber,
			&branch.Photos,
			&branch.DeliveryTarifId,
			&branch.StartTime,
			&branch.EndTime,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &branch, nil
}

func (r *BranchRepo) Update(ctx context.Context, req *user_service.BranchUpdateReq) (*user_service.BranchUpdateResp, error) {
	query := `
    UPDATE branches 
    SET 
        name,
		phone_number,
		photos,
		delivery_tarif_id,
		start_time,
		end_time,
		address,
		destination,
		active
    WHERE id = $1
    `

	_, err := r.db.Exec(ctx, query,
		req.Id,
		req.Name,
		req.PhoneNumber,
		req.Photos,
		req.DeliveryTarifId,
		req.StartTime,
		req.EndTime,
		req.Address,
		req.Destination,
		req.Active,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.BranchUpdateResp{
		Msg: "branch updated",
	}, nil
}

func (r *BranchRepo) Delete(ctx context.Context, req *user_service.BranchIdReq) (*user_service.BranchDeleteResp, error) {
	query := `
    DELETE FROM 
		branches 
    WHERE id = $1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}

	if res.RowsAffected() == 0 {
		return nil, fmt.Errorf("branch not found")
	}

	return &user_service.BranchDeleteResp{
		Msg: "branch deleted",
	}, nil
}
