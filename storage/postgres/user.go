package postgres

import (
	"context"
	"delivery/user_service/genproto/user_service"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *UserRepo {
	return &UserRepo{
		db: db,
	}
}

func (r *UserRepo) Create(ctx context.Context, req *user_service.UserCreateReq) (*user_service.UserCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO users(
		id,
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password
	) VALUES($1,$2,$3,$4,$5,$6,$7);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Active,
		req.Login,
		req.Password,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.UserCreateResp{
		Msg: "user created with id: " + id,
	}, nil
}

func (r *UserRepo) GetList(ctx context.Context, req *user_service.UserGetListReq) (*user_service.UserGetListResp, error) {
	var (
		filter  = " WHERE TRUE"
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password
	FROM users `

	if req.FirstName != "" {
		filter += ` AND first_name='` + req.FirstName + `' `
	}
	if req.LastName != "" {
		filter += ` AND last_name='` + req.LastName + `' `
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ
	// fmt.Println(query)

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	resp := &user_service.UserGetListResp{}
	for rows.Next() {
		var user = user_service.User{}
		err := rows.Scan(
			&user.Id,
			&user.FirstName,
			&user.LastName,
			&user.PhoneNumber,
			&user.Active,
			&user.Login,
			&user.Password,
		)

		if err != nil {
			return nil, err
		}
		resp.Users = append(resp.Users, &user)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *UserRepo) GetById(ctx context.Context, req *user_service.UserIdReq) (*user_service.User, error) {
	query := `
	SELECT 
		id,
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password
	FROM users
	`

	var user = user_service.User{}
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.PhoneNumber,
		&user.Active,
		&user.Login,
		&user.Password,
	); err != nil {
		return nil, err
	}

	return &user, nil
}

func (r *UserRepo) Update(ctx context.Context, req *user_service.UserUpdateReq) (*user_service.UserUpdateResp, error) {
	query := `
	UPDATE users 
	SET 
		first_name,
		last_name,
		phone_number,
		active,
		login,
		password
	WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Active,
		req.Login,
		req.Password,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &user_service.UserUpdateResp{Msg: "OK"}, nil
}

func (r *UserRepo) Delete(ctx context.Context, req *user_service.UserIdReq) (*user_service.UserDeleteResp, error) {
	query := `
    DELETE FROM 
		users
    WHERE id=$1`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &user_service.UserDeleteResp{Msg: "OK"}, nil
}
