package postgres

import (
	"context"
	"delivery/user_service/genproto/user_service"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
)

type ClientRepo struct {
	db *pgxpool.Pool
}

func NewClientRepo(db *pgxpool.Pool) *ClientRepo {
	return &ClientRepo{
		db: db,
	}
}

func (r *ClientRepo) Create(ctx context.Context, req *user_service.ClientCreateReq) (*user_service.ClientCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO clients(
        id,
		first_name,
		last_name,
		phone_number,
		photos,
		active,
		birth_date,
		last_ordered_date,
		total_order_sum,
		total_order_count,
        discount_type,
		discount_amount
	)
	VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Photos,
		req.Active,
		req.BirthDate,
		req.LastOrderedDate,
		req.TotalOrderSum,
		req.TotalOrderCount,
		req.DiscountType,
		req.DiscuontAmount,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &user_service.ClientCreateResp{
		Msg: "client created with id: " + id,
	}, nil
}

func (r *ClientRepo) GetList(ctx context.Context, req *user_service.ClientGetListReq) (*user_service.ClientGetListResp, error) {
	var (
		filter  = " WHERE TRUE "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		first_name,
		last_name,
		phone_number,
		photos,
		active,
		birth_date,
		last_ordered_date,
		total_order_sum,
		total_order_count,
        discount_type,
		discount_amount
	FROM
		clients	
	`

	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	resp := &user_service.ClientGetListResp{}
	for rows.Next() {
		var client = user_service.Client{}
		err := rows.Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.PhoneNumber,
			&client.Photos,
			&client.Active,
			&client.BirthDate,
			&client.LastOrderedDate,
			&client.TotalOrderSum,
			&client.TotalOrderCount,
			&client.DiscountType,
			&client.DiscuontAmount,
		)

		if err != nil {
			return nil, err
		}
		resp.Clients = append(resp.Clients, &client)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *ClientRepo) GetById(ctx context.Context, req *user_service.ClientIdReq) (*user_service.Client, error) {
	query := `
    SELECT 
        id,
		first_name,
		last_name,
		phone_number,
		photos,
		active,
		birth_date,
		last_ordered_date,
		total_order_sum,
		total_order_count,
        discount_type,
		discount_amount
    FROM 
	clients
    WHERE id=$1;
    `

	var client = user_service.Client{}
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		    &client.Id,
			&client.FirstName,
			&client.LastName,
			&client.PhoneNumber,
			&client.Photos,
			&client.Active,
			&client.BirthDate,
			&client.LastOrderedDate,
			&client.TotalOrderSum,
			&client.TotalOrderCount,
			&client.DiscountType,
			&client.DiscuontAmount,
	); err != nil {
		return nil, err
	}

	return &client, nil
}

func (r *ClientRepo) Update(ctx context.Context, req *user_service.ClientUpdateReq) (*user_service.ClientUpdateResp, error) {
	query := `
    UPDATE clients 
    SET 
        first_name,
		last_name,
		phone_number,
		photos,
		active,
		birth_date,
		last_ordered_date,
		total_order_sum,
		total_order_count,
        discount_type,
		discount_amount
    WHERE 
		id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Photos,
		req.Active,
		req.BirthDate,
		req.LastOrderedDate,
		req.TotalOrderSum,
		req.TotalOrderCount,
		req.DiscountType,
		req.DiscuontAmount,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &user_service.ClientUpdateResp{Msg: "OK"}, nil
}

func (r *ClientRepo) Delete(ctx context.Context, req *user_service.ClientIdReq) (*user_service.ClientDeleteResp, error) {
	query := `
	DELETE FROM 
		clients
	WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &user_service.ClientDeleteResp{Msg: "OK"}, nil
}
