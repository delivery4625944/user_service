CREATE TYPE "discount_types" AS ENUM (
  'sum',
  'percent'
);

CREATE TABLE "clients" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar,
  "last_name" varchar,
  "phone_number" varchar,
  "photos" varchar,
  "active" bool DEFAULT true,
  "birth_date" date,
  "last_ordered_date" timestamp,
  "total_order_sum" float,
  "total_order_count" integer,
  "discount_type" discount_types DEFAULT null,
  "discount_amount" numeric,
  "created_at" timestamp DEFAULT 'now()'
);

CREATE TABLE "couriers" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar,
  "last_name" varchar,
  "phone_number" varchar,
  "active" bool DEFAULT true,
  "login" varchar,
  "password" varchar,
  "max_order_count" integer,
  "branch_id" uuid,
  "created_at" timestamp DEFAULT 'now()'
);

CREATE TABLE "users" (
  "id" uuid PRIMARY KEY,
  "first_name" varchar,
  "last_name" varchar,
  "phone_number" varchar,
  "active" bool DEFAULT true,
  "login" varchar,
  "password" varchar,
  "created_at" timestamp DEFAULT 'now()'
);

CREATE TABLE "branches" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "phone_number" varchar,
  "photos" varchar,
  "delivery_tarif_id" uuid,
  "start_time" timestamp,
  "end_time" timestamp,
  "address" varchar,
  "destination" varchar,
  "active" bool DEFAULT true,
  "created_at" timestamp DEFAULT 'now()'
);

ALTER TABLE "couriers" ADD FOREIGN KEY ("branch_id") REFERENCES "branches" ("id");